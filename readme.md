# ConfigReader
Some simple config/ini file reading utilities.

## configreader.py
A wrapper for ConfigParser with a decompress function for decompressing passwords stored as hexed bzips.

### Example Config File ('myconfig.ini')

    [mysection]
    userid=garden
    pwd=425a6839314159265359e1b8b58200000201003a80200030cd00c68022e38bb9229c284870dc5ac100

### Example Usage

    from configreader import ConfigReader
    config = ConfigReader('myconfig.ini')
    config.get('mysection','userid')       # Get normal attribute
    config.decompress('mysection','pwd')   # Get zipped attribute as unziped

### Commandline examples

Generate a hexed bzipped password for use in a config file:

    python3 configreader.py -c "my secret"

Decompress a hexed bzipped password

    python3 configreader.py -d "425a6839314159265359d4bc7853000002118040000a021c202000310c08200f28e021cf8bb9229c28486a5e3c2980"

----

## configparser.js
A simple configparser for Nodejs and Web Browsers.  Returns a dictionary object of the settings.

### Example Node Usage

    const {readConfig} = require('./configparser.js')
    const fs = require('fs')
    let text = fs.readFileSync('myconfig.ini', 'utf8')
    console.log(readConfig(text))