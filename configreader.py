import binascii
import bz2
import configparser
import sys

def hexCompress(string):
    return binascii.hexlify(bz2.compress(bytes(string,'utf8'))).decode('utf8')

def hexDecompress(string):
    return bz2.decompress(binascii.unhexlify(string)).decode('utf8')

class ConfigReader:
    def __init__(self,filename):
        self.config = configparser.ConfigParser()
        self.config.read(filename)
    
    def get(self, section, option):
        return self.config.get(section, option)

    def decompress(self, section, option):
        return hexDecompress(self.config.get(section, option))


if '-c' in sys.argv:
    # compress a string
    print(hexCompress(sys.argv[sys.argv.index('-c')+1]))

if '-d' in sys.argv:
    # compress a string
    print(hexDecompress(sys.argv[sys.argv.index('-d')+1]))