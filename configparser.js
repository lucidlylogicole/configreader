function readConfig(text) {
    let settingD = {}
    let currentD = settingD
    text.split(/\r?\n/).forEach(line=>{
        let linetrim = line.trim()
        if (linetrim.startsWith('[')) {
            // Section
            let section = linetrim.replace(/[\[\]]/gi, '').trim()
            settingD[section] = {}
            currentD = settingD[section]
        } else if (!linetrim.startsWith('#') && linetrim.indexOf('=')>-1) {
            // Get Key / Values
            let spl = linetrim.split('=')
            currentD[spl[0].trim()] = spl.slice(1).join('=').trim()
        }
    })
    return settingD
}

//---
//---Module Exports
if (typeof(module) !== 'undefined') {
    module.exports = {readConfig}
}